package ru.mbakanov.tm.api.repository;

import ru.mbakanov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {
    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clean();
}
