package ru.mbakanov.tm.api.repository;

import ru.mbakanov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clean();

}
