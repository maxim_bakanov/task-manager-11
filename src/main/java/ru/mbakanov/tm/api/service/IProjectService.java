package ru.mbakanov.tm.api.service;

import ru.mbakanov.tm.model.Project;

import java.util.List;

public interface IProjectService {
    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clean();
}
