package ru.mbakanov.tm.controller;

import ru.mbakanov.tm.api.service.IProjectService;
import ru.mbakanov.tm.model.Project;
import ru.mbakanov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements ru.mbakanov.tm.api.controller.IProjectController {

    private IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        for (Project project: projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clean();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[enter name:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description:]");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

}
