package ru.mbakanov.tm.repository;

import ru.mbakanov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements ru.mbakanov.tm.api.repository.IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(Project project) {
        projects.add(project);
    }

    @Override
    public void remove(Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clean() {
        projects.clear();
    }

}
